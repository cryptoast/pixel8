from basics import *
from pixel import *
import mst
import math

MAXDF = 31
STEP = 10

def pxlist_to_freqdic(pxls):
    fd = {}
    for p in pxls:
        try:
            fd[tuple(p)] += 1
        except KeyError:
            fd[tuple(p)] = 1
    return fd

def sortfd(fd):
    return sorted(list(fd.items()),key=lambda x:x[1],reverse=True)

def gen_clusters(fd,step,maxdf):
    clusters = {}
    for k in fd:
        clusters[k] = [k]*fd[k]

    #for thresh in range(step,maxdf,step):
    ballsize = 0
    thresh = 0
    while len(clusters) > maxdf:
        #ballsize += step
        #thresh = ballsize ** (1/3)
        thresh += step
        print('thresh = ' + str(thresh))
        print('clusters = ' + str(len(list(clusters))))
        cqueue = sorted([(k,len(v)) for k,v in clusters.items()],key=lambda x:x[1])
        
        while cqueue != []:
            if len(cqueue) % 100 == 0:
                print(len(cqueue))

            px,sz = cqueue.pop(0)
            scores = sorted([(px2,sz2) for px2,sz2 in cqueue if pxdif(px,px2) <= thresh],
                                key = lambda x:x[1], reverse = True)
            #print(px,scores)

            if len(scores) > 1:
                for i,c in enumerate(scores):
                    if c[0] == px:
                        #print("ping")
                        scores.pop(i)
                        break

                bestkey = scores[0][0]
                bestsz = scores[0][1]

                newpx = ((bestkey[0]*bestsz + px[0]*sz)/(sz + bestsz),
                         (bestkey[1]*bestsz + px[1]*sz)/(sz + bestsz),
                         (bestkey[2]*bestsz + px[2]*sz)/(sz + bestsz))
                '''
                newpx = px
                '''

                newsz = sz + bestsz
                
                try:
                    old1 = clusters.pop(px,[])
                except KeyError:
                    old1 = []

                try:
                    old2 = clusters.pop(bestkey,[])
                except KeyError:
                    old2 = []

                for i,c in enumerate(cqueue):
                    if c[0] == bestkey:
                        cqueue.pop(i)
                        break

                clusters[newpx] = old1 + old2

                cqueue.insert(0,(newpx,newsz))

    return clusters

def save_clusters(clusters):
    outfiles = []
    clust_by_size = sorted(clusters.keys(),key=lambda x:len(clusters[x]),reverse=True)
    for i,clust in enumerate(clust_by_size):
        pixels = clusters[clust]

        defpx = (math.floor(clust[0]),math.floor(clust[1]),math.floor(clust[2]))
        img = snake_fill(pixels,default=defpx)
        #img = smallest_block(pixels)
        '''
        dim = math.ceil(math.sqrt(len(pixels)))
        img = np.zeros((dim,dim,3),dtype=np.uint8)

        row,col = 0,0
        while pixels != []:
            px = pixels.pop()
            img[row][col] = px
            col = (col + 1) % dim
            row = row + 1 if col == 0 else row
        '''

        outfile = 'cluster_' + str(i) + '.png'
        outfiles.append(outfile)

        img = hsl_to_rgb(img)
        save_image(img,outfile)
    return outfiles

def cluster_mst(clusters):
    pxlist = list(sorted(clusters.keys(),key=lambda x:len(clusters[x]),reverse=True))
    numv = len(pxlist)
    g = mst.Graph(len(clusters.keys()))
    for i,v in enumerate(pxlist):
        for j in range(i+1,numv):
            g.addEdge(i,j,pxdif(v,pxlist[j]))

    return pxlist,g.KruskalMST()

def clustersort(img,step=STEP,maxdf=MAXDF):
    pxls = img_to_pxlist(img)
    fd = pxlist_to_freqdic(pxls)
    clusters = gen_clusters(fd,step,maxdf)
    pxl,mst = cluster_mst(clusters)
    clust_files = save_clusters(clusters)
    return clust_files,pxl,mst
