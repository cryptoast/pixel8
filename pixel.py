def pxdif(p1,p2):
    huedf = abs(int(hue(p1)) - int(hue(p2)))
    satdf = abs(int(sat(p1)) - int(sat(p2)))
    lightdf = abs(int(light(p1)) - int(light(p2)))
    df = huedf + (satdf * .8) + (lightdf * .6)
    #print(df)
    return df

def hue(px):
    return px[0]
def light(px):
    return px[1]
def sat(px):
    return px[2]

def red(px):
    return px[2]
def green(px):
    return px[1]
def blue(px):
    return px[0]

def img_to_pxlist(img):
    return [item for sublist in img for item in sublist]

