from basics import *
import numpy as np

def place_at(base,to_place,tl_x,tl_y):
    tpdims = to_place.shape
    base[tl_x:tl_x+tpdims[0]][tl_y:tl_y+tpdims[1]] = to_place
    return base

def can_fit(canvas,item):
    csize,isize = canvas.shape,item.shape
    return csize[0] >= isize[0] and csize[1] >= isize[1]

def remaining_space(canvas,item,rightfirst=True):
    csize,isize = canvas.shape,item.shape

    spaces = []
    if csize == isize:
        return spaces

    hdif = csize[0] - isize[0]
    vdif = csize[1] - isize[1]

    if hdif == 0:
        spaces.append((0,csize[0],isize[1],csize[1]))
    elif vdif == 0:
        spaces.append((isize[0],csize[0],0,csiz[1]))
    elif hdif >= vdif:
        spaces.append((isize[0],csize[0],0,csize[1]))
        spaces.append((0,isize[0],isize[1],csize[1]))
    else:
        spaces.append((0,csize[0],isize[1],csize[1]))
        spaces.append((isize[0],csize[0],0,isize[1]))

    return spaces

# imgs must be sorted from largest to smallest
def tile(imgs):
    pass
