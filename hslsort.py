import numpy as np
import cv2
import math
from basics import *
from graph2 import *
from pixel import *

# NOTES
#  - img assumes a (w x h x 3) ndarray of HSV values

def pxgraph(pxlist):
    g = Graph()
    for i in range(len(pxlist)):
        g.add_node(i)

    for i in range(len(pxlist)):
        for j in range(len(pxlist)):
            if i != j:
                g.add_edge(i,j,pxdif(pxlist[i],pxlist[j]))

    return g
            
def sort(pxlist):
    g = pxgraph(pxlist)
    v, path = dijkstra(g,0)
    print(v)
    print(path)
    return path

def sort2(origlist):
    pxlist = origlist.copy()
    cur = pxlist.pop(0)
    outlist = [cur]
    while pxlist != []:
        closest = None
        diff = -1
        for i,v in enumerate(pxlist):
            if closest == None:
                closest = i
                diff = pxdif(cur,v)
            else:
                d = pxdif(cur,v)
                if d < diff:
                    closest = i
                    diff = d
        outlist.append(pxlist.pop(closest))
    return outlist

def imgsort(img):
    return sort(imt_to_pxlist(img))
