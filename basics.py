import numpy as np
import cv2, math, random

def load_image(filename):
    im = cv2.imread(filename)
    print("image loaded. dims : ", str(im.shape))
    return im

def save_image(imarray,filename):
    print("saving image as %s..." % filename)
    cv2.imwrite(filename,imarray,(cv2.IMWRITE_PNG_COMPRESSION,0))
    print("file successfully saved!")
    return

def jpg_to_png(infile,outfile):
    im = cv2.imread(infile)
    save_image(im,outfile)
    return

def rgb_to_hsl(img):
    return cv2.cvtColor(img,cv2.COLOR_BGR2HLS)

def hsl_to_rgb(img):
    return cv2.cvtColor(img,cv2.COLOR_HLS2BGR)

def enlarge_img(oldimg,scale):
    olddims = oldimg.shape
    newdims = (olddims[0]*scale,olddims[1]*scale,3)
    newimg = np.zeros(newdims)
    
    for i in range(newdims[0]):
        for j in range(newdims[1]):
            x = i // scale
            y = j // scale
            newimg[i][j] = oldimg[x][y]
    return newimg

def enlarge_file(oldfile,scale,rootdir='./',prefix='enl_'):
    oldimg = load_image(rootdir + oldfile)
    newimg = enlarge_img(oldimg,scale)
    save_image(newimg,rootdir + prefix + oldfile)
    return

def shuffle_img(oldimg):
    dims = oldimg.shape
    newimg = np.zeros(dims)
    numpix = dims[0]*dims[1]

    indices = list(range(numpix))
    random.shuffle(indices)
    for i,j in enumerate(indices):
        oldx,oldy = i // dims[0], i % dims[1]
        newx,newy = j // dims[0], j % dims[1]
        newimg[newx][newy] = oldimg[oldx][oldy]
    return newimg

def shuffle_file(oldfile,rootdir='./',prefix='shuf_'):
    oldimg = load_image(rootdir + oldfile)
    newimg = shuffle_img(oldimg)
    save_image(newimg,rootdir + prefix + oldfile)
    return

def reduce_quality_img(oldimg,step):
    dims = oldimg.shape
    newimg = np.zeros(dims)
    for i in range(dims[0]):
        for j in range(dims[1]):
            oldpx = oldimg[i][j]
            newb = oldpx[0] - (oldpx[0] % step)
            newg = oldpx[1] - (oldpx[1] % step)
            newr = oldpx[2] - (oldpx[2] % step)
            newimg[i][j] = (newb,newg,newr)
    return newimg

def reduce_quality_file(oldfile,step,rootdir='./',prefix='lq_'):
    oldimg = load_image(rootdir + oldfile)
    newimg = reduce_quality_img(oldimg,step)
    save_image(newimg,rootdir + prefix + oldfile)
    return

def snake_fill(pxlist,default=(0,0,0)):
    dims = int(math.ceil(math.sqrt(len(pxlist))))
    newimg = np.zeros((dims,dims,3),dtype=np.uint8)
    for i in range(dims):
        for j in range(dims):
            newimg[i][j] = default

    try:
        for i in range(dims):
            ccw = (i % 2 == 0)

            if ccw:
                # row l to r
                for x in range(i):
                    newimg[i][x] = pxlist.pop()

                # diag
                newimg[i][i] = pxlist.pop()

                # column b to t
                for x in range(i):
                    newimg[i-x-1][i] = pxlist.pop()

            else:
                # column t to b
                for x in range(i):
                    newimg[x][i] = pxlist.pop()

                # diag
                newimg[i][i] = pxlist.pop()

                # row r to l
                for x in range(i):
                    newimg[i][i-x-1] = pxlist.pop()
    except IndexError:
        pass

    return newimg

def smallest_block(pxlist):
    pxlen = len(pxlist)
    sqrdim = int(math.ceil(math.sqrt(pxlen)))
    height,width = pxlen,1
    for i in range(sqrdim,0,-1):
        if pxlen % i == 0:
            width = i
            height = pxlen // i
            break

    newimg = np.zeros((width,height,3),dtype=np.uint8)
    for i in range(pxlen):
        newimg[i % width][i // width] = pxlist[i]

    return newimg
