from basics import *
import sort2
from sys import argv
import cv2

if len(argv) >= 4:
    infile = argv[3]
else:
    infile = input('infile: ')

img = cv2.imread(infile)
img = rgb_to_hsl(img)

if len(argv) >= 3:
    clust_files,_,edges = sort2.clustersort(img,step=int(argv[1]),maxdf=int(argv[2]))
else:
    clust_files,_,edges = sort2.clustersort(img)

for c in clust_files:
    shuffle_file(c)
    #enlarge_file('shuf_' + c,16)

with open('edges.txt','w+') as f:
    for a,b,_ in edges:
        f.write('%d,%d\n' % (a,b))

